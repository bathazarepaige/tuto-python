recette = {
    "nom": "Tarte aux pommes",
    "temps": 30,
    "ingredients": ["pommes", "..."],
    "etapes": {
        "preparation": ["couper des pommes", "..."],
        "cuisson": ["cuire au four à 200° pendant 30min", "..."]
    },
}

recette["nom"] = "Autre chose"
recette["niveau"] = "débutant"

recette.update({
    "nom": "Tarte au chocolat",
    "type": "dessert"
})

del recette["temps"]

niveau = recette.pop("niveau")

print(niveau)
print(recette)

for cle in recette:
    print(cle)


for cle, valeur in recette.items():
    print(f"La clé est {cle} et la valeur est {valeur}")

recette["origine"] = "France"
print(recette.get("origine", "Italie"))